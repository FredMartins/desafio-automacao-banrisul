package suporte;

import java.io.File;

public class CriarPasta {
	public static void criarPastaRelatorio(String caminho) {
		File caminhoRelatorio = new File(caminho);
		if(!caminhoRelatorio.exists()) {
			caminhoRelatorio.mkdir();
		}
	}
}
