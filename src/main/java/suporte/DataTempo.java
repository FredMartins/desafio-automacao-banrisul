package suporte;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DataTempo {
	public static String pegarDataNoFormatoDoRelatorio() {
		SimpleDateFormat formatoData = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Date agora = new Date();
		return formatoData.format(agora);
	}
	
	public static String pegarDataNoFormatoDaCapturaDeTela() {
		DateTimeFormatter formatadorData = DateTimeFormatter.ofPattern("dd-MM-yyyy_hh'h'mm'm'ss's");
		LocalDateTime dataLocal = LocalDateTime.now();
		return formatadorData.format(dataLocal);
	}
}
