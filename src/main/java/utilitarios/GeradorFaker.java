package utilitarios;

import java.util.Locale;

import com.github.javafaker.Faker;

public class GeradorFaker {
	private Faker faker;
	
	public GeradorFaker() {
		faker = new Faker(new Locale("en-US"));
	}
	
	public String pegaPrimeiroNome() {
		return faker.name().firstName();
	}
	
	public String pegarSobrenome() {
		return faker.name().lastName();
	}
	
	public String pegarCEP() {
		return faker.address().zipCode();
	}
	
	public String pegarEmail() {
		return faker.internet().safeEmailAddress();
	}
	
	public String pegarSenha() {
		return faker.internet().password();
	}
	
	public String pegarEndereco() {
		return faker.address().streetAddress();
	}
	
	public String pegarTelefone() {
		return faker.phoneNumber().cellPhone();
	}
	
	public String pegarCidade() {
		return faker.address().city();
	}
}
