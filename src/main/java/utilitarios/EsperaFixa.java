package utilitarios;

public class EsperaFixa {
	public static void esperaEmSegundos(int segundos) {
		try {
			Thread.sleep(segundos*1000);
		}catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
}
