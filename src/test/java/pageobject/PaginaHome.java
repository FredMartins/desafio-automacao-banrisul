package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaHome {
	private WebDriver driver;
	
	private Esperas espera;
	
	public PaginaHome(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	/*
	 * Busca o produto da lista pelo nome
	 */
	public WebElement pegarProdutoPorNome() {
		WebElement nomeProduto = driver.findElement(By.linkText("Faded Short Sleeve T-shirts"));
		return espera.esperaCarregarElemento(nomeProduto);
	}
}
