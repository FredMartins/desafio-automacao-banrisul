package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaCarrinho {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaCarrinho(WebDriver driver) {
		espera = new Esperas(driver);
		this.driver = driver;
	}
	
	public WebElement pegarNomeProduto() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.linkText("Faded Short Sleeve T-shirts"));
	}
	
	public WebElement pegarBotaoFazerCheckout() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//p[@class='cart_navigation clearfix']/a[@title='Proceed to checkout']"));
	}
	
	public WebElement pegarTituloDaPagina() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.className("page-heading"));
	}
}
