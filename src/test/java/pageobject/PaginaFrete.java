package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaFrete {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaFrete(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	public WebElement pegarBotaoProsseguir() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//p[@class='cart_navigation clearfix']/button"));
	}
	
	public WebElement pegarCheckboxTermos() {
		WebElement elemento = driver.findElement(By.xpath("//p[@class='checkbox']/div[@class='checker']/span/input"));
		return espera.esperaCarregarElemento(elemento);
	}
}
