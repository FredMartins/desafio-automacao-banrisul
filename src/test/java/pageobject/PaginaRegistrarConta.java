package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utilitarios.Esperas;

public class PaginaRegistrarConta {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaRegistrarConta(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	public WebElement pegarInputNome() {
		WebElement inputNome = driver.findElement(By.id("customer_firstname"));
		return espera.esperaCarregarElemento(inputNome);
	}
	
	public WebElement pegarInputSobrenome() {
		WebElement inputSobrenome = driver.findElement(By.id("customer_lastname"));
		return espera.esperaCarregarElemento(inputSobrenome);
	}
	
	public WebElement pegarInputSenha() {
		WebElement inputSenha = driver.findElement(By.id("passwd"));
		return espera.esperaCarregarElemento(inputSenha);
	}
	
	public WebElement pegarInputEndereco() {
		WebElement inputEndereco = driver.findElement(By.id("address1"));
		return espera.esperaCarregarElemento(inputEndereco);
	}
	
	public WebElement pegarInputCidade() {
		WebElement inputCidade = driver.findElement(By.id("city"));
		return espera.esperaCarregarElemento(inputCidade);
	}
	
	public WebElement pegarInputCEP() {
		WebElement inputCEP = driver.findElement(By.id("postcode"));
		return espera.esperaCarregarElemento(inputCEP);
	}
	
	public WebElement pegarInputTelefone() {
		WebElement inputTelefone = driver.findElement(By.id("phone_mobile"));
		return espera.esperaCarregarElemento(inputTelefone);
	}
	
	public WebElement pegarBotaoRegistro() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.id("submitAccount"));
	}
	
	public WebElement pegarTituloPagina() {
		WebElement tituloPagina = driver.findElement(By.xpath("//div[@id='center_column']"
															  +"/div[@id='noSlide']"
															  +"/h1[@class='page-heading']"));
		return espera.esperaCarregarElemento(tituloPagina);
	}
	
	public Select pegarEstadoDropdown() {
		Select dropdown = new Select(driver.findElement(By.id("id_state")));
		return dropdown;
	}
}
