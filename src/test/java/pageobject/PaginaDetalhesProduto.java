package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaDetalhesProduto {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaDetalhesProduto(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	public WebElement pegarBotaoAdicionarNoCarrinho() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//p[@id='add_to_cart']/button"));
	}
	
	public WebElement pegarNomeProduto() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.tagName("h1"));
	}
	
	public WebElement pegarBotaoCheckout() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//div[@class='button-container']/a[@title='Proceed to checkout']"));
	}
}
