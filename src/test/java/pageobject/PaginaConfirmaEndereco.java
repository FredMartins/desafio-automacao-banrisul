package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaConfirmaEndereco {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaConfirmaEndereco(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	public WebElement pegarTituloDaPagina() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.className("page-heading"));
	}
	
	public WebElement pegarBotaoProsseguir() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//p[@class='cart_navigation clearfix']/button"));
	}
}
