package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaPagamento {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaPagamento(WebDriver driver) {
		this.driver = driver;
		espera = new Esperas(driver);
	}

	public WebElement pegarValorTotal() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.id("total_price"));
	}
	
	public WebElement pegarValorFrete() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.id("total_shipping"));
	}
	
	public WebElement pegarTotalProduto() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.id("total_product"));
	}
	
	public WebElement pegarPagamentoCartao() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//div[@id='HOOK_PAYMENT']"
																	+ "/div[@class='row']"
																	+ "/div[@class='col-xs-12 col-md-6']"
																	+ "/p[@class='payment_module']"
																	+ "/a[@class='bankwire']"));
	}
	
	public WebElement pegarBotaoConfirmaCompra() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//p[@id='cart_navigation']/button"));
	}
	
	public WebElement pegarConfirmacaoCompra() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.xpath("//div[@class='box']"
																	+ "/p[@class='cheque-indent']"
																	+ "/strong"));
	}
}
