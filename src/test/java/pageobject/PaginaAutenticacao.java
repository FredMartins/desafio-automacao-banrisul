package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilitarios.Esperas;

public class PaginaAutenticacao {
	private WebDriver driver;
	private Esperas espera;
	
	public PaginaAutenticacao(WebDriver driver){
		this.driver = driver;
		espera = new Esperas(driver);
	}
	
	public WebElement pegarInputEmail() {
		WebElement inputEmail = driver.findElement(By.id("email_create"));
		return espera.esperaCarregarElemento(inputEmail);
	}
	
	public WebElement pegarBotaoCriarConta() {
		return espera.esperaVisibilidadeDoElementoMapeado(By.id("SubmitCreate"));
	}
	
	public WebElement pegarTituloPagina() {
		WebElement tituloPagina = driver.findElement(By.xpath("//div[@id='center_column']/h1[@class='page-heading']"));
		return espera.esperaCarregarElemento(tituloPagina);
	}
}
