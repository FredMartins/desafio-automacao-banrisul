package casosteste;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import suporte.CapturaDeTela;
import suporte.Relatorio;
import suporte.TesteBase;
import suporte.TipoRelatorio;
import tarefas.AutenticacaoTarefas;
import tarefas.CarrinhoTarefas;
import tarefas.ConfirmarEnderecoTarefas;
import tarefas.DetalheProdutoTarefas;
import tarefas.FreteTarefas;
import tarefas.HomeTarefas;
import tarefas.PagamentoTarefas;
import tarefas.RegistrarContaTarefas;
import utilitarios.EsperaFixa;

public class RealizarCompra extends TesteBase{
	private WebDriver driver = this.pegarDriver();
	HomeTarefas home = new HomeTarefas(driver);
	DetalheProdutoTarefas detalhesProduto = new DetalheProdutoTarefas(driver);
	CarrinhoTarefas carrinhoTarefas = new CarrinhoTarefas(driver);
	AutenticacaoTarefas autenticacao = new AutenticacaoTarefas(driver);
	RegistrarContaTarefas registrarConta = new RegistrarContaTarefas(driver);
	ConfirmarEnderecoTarefas endereco = new ConfirmarEnderecoTarefas(driver);
	FreteTarefas frete = new FreteTarefas(driver);
	PagamentoTarefas pagamento = new PagamentoTarefas(driver);
	
	@Test
	public void realizarCompraComSucesso() throws InterruptedException {
		try {
			Relatorio.criaTeste("Realizar Compra com Sucesso", TipoRelatorio.SINGLE);
			
			EsperaFixa.esperaEmSegundos(2);
			home.selecionaProduto();
			EsperaFixa.esperaEmSegundos(2);
			detalhesProduto.adicionaProdutoCarrinho();
			EsperaFixa.esperaEmSegundos(2);
			carrinhoTarefas.realizarCheckout();
			EsperaFixa.esperaEmSegundos(2);
			autenticacao.realizarCadastro();
			EsperaFixa.esperaEmSegundos(4);
			registrarConta.realizarRegistro();
			EsperaFixa.esperaEmSegundos(2);
			endereco.prosseguirComCompra();
			EsperaFixa.esperaEmSegundos(2);
			frete.aceitarTermosServico();
			EsperaFixa.esperaEmSegundos(2);
			pagamento.confirmarPagamento();
			EsperaFixa.esperaEmSegundos(3);
		}catch(Exception e) {
			Relatorio.log(Status.ERROR, e.getMessage(),CapturaDeTela.fullPageBase64(driver));
		}
	}
	
	/*
	@Test
	public void carregaGoogle() throws InterruptedException {
		driver.get("http://www.google.com");
		WebElement searchbox = driver.findElement(By.name("q"));
		Thread.sleep(2000);
		searchbox.sendKeys("pokémon");
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.className("gNO89b"));
		searchBtn.click();
	}
	*/
}
