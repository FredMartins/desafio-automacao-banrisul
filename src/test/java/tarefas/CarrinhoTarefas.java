package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaCarrinho;
import suporte.CapturaDeTela;
import suporte.Relatorio;

public class CarrinhoTarefas {
	private static WebDriver driver;
	private static PaginaCarrinho paginaCarrinho;
	
	public CarrinhoTarefas(WebDriver driver) {
		this.driver = driver;
		paginaCarrinho = new PaginaCarrinho(this.driver);
	}
	
	public void realizarCheckout() {
		validaProdutoCarrinho();
		paginaCarrinho.pegarBotaoFazerCheckout().click();
		validaPaginaDeAutenticacao();
	}
	
	public void validaProdutoCarrinho() {
		Assertions.assertTrue(paginaCarrinho.pegarNomeProduto().isEnabled());
		Assertions.assertEquals(paginaCarrinho.pegarNomeProduto().getText(), "Faded Short Sleeve T-shirts");
	}
	
	public void validaPaginaDeAutenticacao() {
		try {

			Assertions.assertEquals(paginaCarrinho.pegarTituloDaPagina().getText(), "AUTHENTICATION");
			Relatorio.log(Status.PASS, "Autenticação iniciada com sucesso");
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Autenticação não foi iniciada");
		}
	}
}
