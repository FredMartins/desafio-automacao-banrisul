package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaConfirmaEndereco;
import suporte.CapturaDeTela;
import suporte.Relatorio;

public class ConfirmarEnderecoTarefas {
	private static WebDriver driver;
	private static PaginaConfirmaEndereco endereco;
	
	public ConfirmarEnderecoTarefas(WebDriver driver) {
		this.driver = driver;
		endereco = new PaginaConfirmaEndereco(this.driver);
	}
	
	public void prosseguirComCompra() {
		validaPaginaEndereco();
		endereco.pegarBotaoProsseguir().click();
		validaPaginaFrete();
	}
	
	public void validaPaginaEndereco() {
		try {
			Assertions.assertTrue(endereco.pegarTituloDaPagina().isDisplayed());
			Assertions.assertEquals(endereco.pegarTituloDaPagina().getText(),"ADDRESSES");
			Relatorio.log(Status.PASS, "Cadastro realizado com sucesso", CapturaDeTela.fullPageBase64(driver));
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Cadastro não foi realizado", CapturaDeTela.fullPageBase64(driver));
		}
		
	}
	
	public void validaPaginaFrete() {
		try {
			Assertions.assertTrue(endereco.pegarTituloDaPagina().isDisplayed());
			Assertions.assertEquals(endereco.pegarTituloDaPagina().getText(),"SHIPPING");
			Relatorio.log(Status.PASS, "Pagina de frete acessada com sucesso", CapturaDeTela.fullPageBase64(driver));
		}catch(Exception e) {
			Relatorio.log(Status.FAIL, "Pagina de frete não foi acessada", CapturaDeTela.fullPageBase64(driver));
		}
		
	}
}
