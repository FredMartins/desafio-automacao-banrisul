package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaDetalhesProduto;
import pageobject.PaginaHome;
import suporte.CapturaDeTela;
import suporte.Relatorio;

public class HomeTarefas {
	private static WebDriver driver;
	private static PaginaHome paginaHome;
	private static PaginaDetalhesProduto detalheProduto;
	
	public HomeTarefas(WebDriver driver) {
		this.driver = driver;
		paginaHome = new PaginaHome(this.driver);
		detalheProduto = new PaginaDetalhesProduto(this.driver);
	}
	
	public void selecionaProduto() {
		paginaHome.pegarProdutoPorNome().click();
		validaProdutoSelecionado();
	}
	
	public void validaProdutoSelecionado() {
		try {
			Assertions.assertTrue(detalheProduto.pegarNomeProduto().isDisplayed());
			Relatorio.log(Status.PASS, "Produto selecionado com sucesso", CapturaDeTela.fullPageBase64(driver));
		}catch(Exception e) {
			Relatorio.log(Status.FAIL, "Produto não selecionado", CapturaDeTela.fullPageBase64(driver));
		}
	}
}
