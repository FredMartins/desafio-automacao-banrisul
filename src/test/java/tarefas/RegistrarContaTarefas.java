package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaRegistrarConta;
import suporte.CapturaDeTela;
import suporte.Relatorio;
import utilitarios.GeradorFaker;

public class RegistrarContaTarefas {
	private static WebDriver driver;
	private static PaginaRegistrarConta registro;
	private static GeradorFaker geradorFaker;
	
	public RegistrarContaTarefas(WebDriver driver) {
		this.driver = driver;
		registro = new PaginaRegistrarConta(this.driver);
		geradorFaker = new GeradorFaker();
	}
	
	public void realizarRegistro() {
		verificaTituloPagina();
		registro.pegarInputNome().sendKeys(geradorFaker.pegaPrimeiroNome());
		registro.pegarInputSobrenome().sendKeys(geradorFaker.pegarSobrenome());
		registro.pegarInputSenha().sendKeys(geradorFaker.pegarSenha());
		registro.pegarInputEndereco().sendKeys(geradorFaker.pegarEndereco());
		registro.pegarInputCidade().sendKeys(geradorFaker.pegarCidade());
		trocaEstadoDropdown();
		registro.pegarInputCEP().sendKeys("00000");
		registro.pegarInputTelefone().sendKeys(geradorFaker.pegarTelefone());
		registro.pegarBotaoRegistro().click();
	}
	
	public void verificaTituloPagina() {
		try {
			Assertions.assertTrue(registro.pegarTituloPagina().isEnabled());
			Assertions.assertEquals(registro.pegarTituloPagina().getText(),"CREATE AN ACCOUNT");
			Relatorio.log(Status.PASS, "Registro de nova conta inicializada com sucesso",CapturaDeTela.fullPageBase64(driver));
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Registro de nova conta não foi inicializada",CapturaDeTela.fullPageBase64(driver));
		}
	}
	
	public void trocaEstadoDropdown() {
		registro.pegarEstadoDropdown().selectByValue("5");
	}
}
