package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaDetalhesProduto;
import suporte.CapturaDeTela;
import suporte.Relatorio;

public class DetalheProdutoTarefas {
	private static WebDriver driver;
	private static PaginaDetalhesProduto detalhesProduto;
	
	public DetalheProdutoTarefas(WebDriver driver) {
		this.driver = driver;
		detalhesProduto = new PaginaDetalhesProduto(this.driver);
	}
	
	public void adicionaProdutoCarrinho() {
		detalhesProduto.pegarBotaoAdicionarNoCarrinho().click();
		validaAdicionadoAoCarrinho();
		detalhesProduto.pegarBotaoCheckout().click();
	}
	
	public void validaAdicionadoAoCarrinho() {
		try {
			Assertions.assertTrue(detalhesProduto.pegarBotaoCheckout().isEnabled());
			Relatorio.log(Status.PASS, "Produto adicionado com sucesso", CapturaDeTela.fullPageBase64(driver));
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Produto não foi adicionado", CapturaDeTela.fullPageBase64(driver));
		}
	}
	
	
}
