package tarefas;

import org.openqa.selenium.WebDriver;

import pageobject.PaginaFrete;

public class FreteTarefas {
	private static WebDriver driver;
	private static PaginaFrete frete;
	
	public FreteTarefas(WebDriver driver) {
		this.driver = driver;
		frete = new PaginaFrete(this.driver);
	}
	
	public void aceitarTermosServico() {
		frete.pegarCheckboxTermos().click();
		frete.pegarBotaoProsseguir().click();
	}
}
