package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaAutenticacao;
import suporte.CapturaDeTela;
import suporte.Relatorio;
import utilitarios.GeradorFaker;

public class AutenticacaoTarefas {
	private static WebDriver driver;
	private static PaginaAutenticacao autenticacao;
	private static String email;
	private GeradorFaker faker;
	
	public AutenticacaoTarefas(WebDriver driver) {
		this.driver = driver;
		autenticacao = new PaginaAutenticacao(driver);
		faker = new GeradorFaker();
		email = faker.pegarEmail();
	}
	
	public void realizarCadastro() {
		validaPaginaAutenticacao();
		autenticacao.pegarInputEmail().sendKeys(email);
		verificaBotaoVisivel();
		autenticacao.pegarBotaoCriarConta().click();
		
	}
	
	public void verificaBotaoVisivel() {
		try {
			Assertions.assertTrue(autenticacao.pegarBotaoCriarConta().isDisplayed());
			Relatorio.log(Status.PASS, "Botão de criar conta está visível");
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Botão de criar conta não foi encontrado");
		}
	}
	
	public void validaPaginaAutenticacao() {
		try {
			Assertions.assertTrue(autenticacao.pegarTituloPagina().isDisplayed());
			Assertions.assertEquals(autenticacao.pegarTituloPagina().getText(), "AUTHENTICATION");
			Relatorio.log(Status.PASS, "Página de autenticação acessada com sucesso", CapturaDeTela.fullPageBase64(driver));
		} catch (Exception e) {
			Relatorio.log(Status.FAIL, "Página de autenticação não foi acessada", CapturaDeTela.fullPageBase64(driver));
		}
	}
	
}
