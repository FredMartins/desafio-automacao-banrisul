package tarefas;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import pageobject.PaginaPagamento;
import suporte.CapturaDeTela;
import suporte.Relatorio;

public class PagamentoTarefas {
	private static WebDriver driver;
	private static PaginaPagamento pagamento;
	
	public PagamentoTarefas(WebDriver driver) {
		this.driver = driver;
		pagamento = new PaginaPagamento(this.driver);
	}
	
	public void confirmarPagamento() {
		validaValorTotal();
		pagamento.pegarPagamentoCartao().click();
		pagamento.pegarBotaoConfirmaCompra().click();
		validaCompra();
	}
	
	public String calculaTotal() {
		double precoProduto = Double.parseDouble(pagamento.pegarTotalProduto().getText().substring(1));
		double frete = Double.parseDouble(pagamento.pegarValorFrete().getText().substring(1));
		double total = precoProduto + frete;
		return "$"+total;
	}
	
	public void validaValorTotal() {
		try {
			Assertions.assertTrue(pagamento.pegarValorTotal().isDisplayed());
			Assertions.assertEquals(pagamento.pegarValorTotal().getText(),calculaTotal());
			Relatorio.log(Status.PASS, "Valor total da compra validado com sucesso", CapturaDeTela.fullPageBase64(driver));
		} catch (Exception e) {	
			Relatorio.log(Status.FAIL, "Valor total da compra não foi validado", CapturaDeTela.fullPageBase64(driver));
		}
	}
	
	public void validaCompra() {
		try {
			Assertions.assertTrue(pagamento.pegarConfirmacaoCompra().isDisplayed());
			Assertions.assertEquals(pagamento.pegarConfirmacaoCompra().getText(), "Your order on My Store is complete.");
			Relatorio.log(Status.PASS, "Compra realizada com sucesso", CapturaDeTela.fullPageBase64(driver));
		}catch(Exception e) {
			Relatorio.log(Status.PASS, "Não foi possivel realizar a compra", CapturaDeTela.fullPageBase64(driver));
		}
		
	}
}
